// middleware function
function verifyToken(req, res, next) {
  const bearerHeader = req.headers["authorization"];
  if (!bearerHeader) {
    res.sendStatus(403);
    return;
  }
  const bearerToken = bearerHeader.split(" ")[1];
  req.token = bearerToken;
  next();
}

module.exports = verifyToken;
