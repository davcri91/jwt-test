const express = require("express");
const bodyParser = require("body-parser");
const verifyToken = require("./vertify-token");
const jwt = require("jsonwebtoken");

const jsonParser = bodyParser.json();
const secretKey = "secretkey";
const app = express();

const spots = [
  { id: 1, name: "Microsoft" },
  { id: 2, name: "Google" },
];

app.get("/api/spots", verifyToken, (req, res) => {
  jwt.verify(req.token, secretKey, (err, authData) => {
    if (err) {
      console.log("Error: " + err.message);
      res.sendStatus(403);
    } else {
      res.json({
        spots,
        authData,
      });
    }
  });
});

app.post("/api/spots", jsonParser, verifyToken, (req, res) => {
  jwt.verify(req.token, secretKey, (err, authData) => {
    if (err) {
      console.log("Error: " + err.message);
      res.sendStatus(403);
    } else {
      const spot = {
        name: req.body.name,
        id: spots.length + 1,
      };
      spots.push(spot);
      res.json({
        spots,
        authData,
      });
    }
  });
});

app.listen(6000, () => {
  console.log("Server started");
});
