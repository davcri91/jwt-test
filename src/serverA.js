const express = require("express");
const jwt = require("jsonwebtoken");
const verifyToken = require("./vertify-token");
const secretKey = "secretkey";
const app = express();

app.get("/api", (req, res) => {
  res.json({
    message: "Welcome to the API",
  });
});

app.post("/api/posts", verifyToken, (req, res) => {
  jwt.verify(req.token, secretKey, (err, authData) => {
    if (err) {
      console.log("Error: " + err.message);
      res.sendStatus(403);
    } else {
      res.json({
        message: "Post created...",
        authData,
      });
    }
  });
});

app.post("/api/login", (req, res) => {
  // Mock user. Real user should be passed in the req
  const user = {
    id: 1,
    username: "pippo",
    email: "pippo@mail.com",
  };
  jwt.sign({ user }, secretKey, { expiresIn: "1020s" }, (err, token) => {
    res.json({
      token,
    });
  });
});

app.listen(5000, () => {
  console.log("Server started");
});
