# JWT test

## Run

```sh
npm install
npm run serverA
# and in another terminal
npm run serverB
```

- **Server A** is for the login: emits the JWT token
- **Server B** is to access resources

## Run the HTTP calls

1. Install VSCode
2. Install [REST client](https://marketplace.visualstudio.com/items?itemName=humao.rest-client)
3. Open ./src/api.rest
4. Click on "Send request" for the desired HTTP call

## Useful links

- https://jwt.io/

## Video example

[./example.mp4](./example.mp4)
